/*

I spliced a display extension cable and used arduino Mega to intercept
serial communications.

cable I bought from http://www.ebay.com/itm/8fun-bafang-center-motor-mid-drive-motor-kit-display-extension-cable-/322187825892?var=&hash=item4b03e412e4:m:mF0vjpnXzTANDTToiH3yXNw

white: display tx, controller rx. 
green: display rx, controller tx.

//software capture
Black (GND) arduino gnd, 
Green (TXD) 5v->3.3v, serial3RX, 
White (RXD) 3.3v->5v, serial3TX,
Brown (P+)  Orange (PL)


due needs custom firmware for 16u2
*/
void setup() {
  pinMode(13, OUTPUT);
  digitalWrite(13, false);
  // put your setup code here, to run once:
  delay(200);
  Serial.begin(1200);    //usb serial i/o
  Serial3.begin(1200);   //bafang controller
  /*
	using mega adk board
	tx0,1,2 cause controller to stop sending bytes??
	probally cause their outputs not inputs
	
	hook rx0 to controller rx
	rx3 to controller tx
  */
  SerialUSB.begin(115200);  
  delay(50);

  SerialUSB.println("begin session");
}

boolean state = false;
void toggle_led(){
  if(state){
    digitalWrite(13, false);
    state = false;
  } else {
    digitalWrite(13, true);   
    state = true;    
  }
  //delay(30);
}

void logHEX(int b){
  if(b < 16)
    SerialUSB.print("0");
  SerialUSB.print(b, HEX);
  SerialUSB.print("\n");  
}
void logTX(int b){
  SerialUSB.print("tx: ");
  logHEX(b);
}
void logRX(int b){
  SerialUSB.print("rx: ");
  logHEX(b);  
}
void serial_Event(int s2b){
  //int s2b = Serial.read();
  if(s2b > 0){
    Serial3.write(s2b);
    logRX(s2b);
  }
}


void serial_Event3(int s1b){
  //int s1b = Serial3.read();
  if(s1b > 0){
    Serial.write(s1b);
    logTX(s1b);
  }
}

void serialUSB_Event(int b){
  if(b > 0){
    
  }
}
void loop() {
 /*
  * Read bytes as fast as possible one at a time from each port,
  * this ensures data is synced as close as possible.
  */
  /*if(Serial2.available() > 0){
    byte b = Serial2.read();
	  Serial.write(b);
    Serial2.write(b);
  } else {
    Serial2.write(0);
  }*/
  //Serial1.println("begin session");
  digitalWrite(13, true); 
  int s2b = Serial.read();
  serial_Event(s2b);
  s2b = Serial3.read();
  serial_Event3(s2b);
  
  //s2b = SerialUSB.read();
  //serialUSB_Event(s2b);
}
