
Have to modify Due Firmware to not reset into bootloader mode when using 1200 baud???

Arduino DUE instructions

mods initially from
https://petervanhoyweghen.wordpress.com/2013/05/04/disabling-auto-reset-on-the-due/

Install pin header to PB5 PB7

![](due_16u2_gpio.jpg "")

connect USBTinyISP to pin header and run avrdude

avrdude -c usbtiny -p m16u2 -U flash:w:Arduino-usbserial_16u2_due.hex:i


when programming sketch via Arduino disconnect native port
and install remove jumper on PB5 PB7

install jumper to connect without resetting/erasing due board at 1200  baud

due needs level shift to talk to bbs02 controller

![](3v_level_shift.png "")

Serial3Tx -> Rx-In -> Rx-out -> white

green -> Tx-In -> Tx-out -> Serial3Rx


When done due can be used with node_logger here to capture serial bytes from 8f software
or display/controller connection

//software capture

Black (GND) arduino gnd, 

Green (TXD) 5v->3.3v, serial3RX, 

White (RXD) 3.3v->5v, serial3TX,

Brown (P+)  Orange (PL)


//display capture

Black (GND) arduino gnd, Black

Green (TXD) 5v->3.3v, serial3RX, Green

White (RXD) 5v->3.3v, serial2RX, White

Brown (P+)  Brown

Orange (PL) Orange

![](IMAG0028.jpg "")


