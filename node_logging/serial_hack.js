var tty = require("serialport");
var prompt = require('prompt');

var serialPort = null;
var baudrate = 1200;

function open_serial_port(name){
	try {
		console.log("opening "+name);
  		serialPort = new tty.SerialPort(name, {
  			baudrate: baudrate,
			bufferSize: 4096
		});
		serialPort.on("open", function () {
			var overflow_string = "";
  			serialPort.on('data', function(data) {
				console.log(data);
    			/*var a = data.toString().split(/\n/);
				if(overflow_string.length > 0){
					a = overflow_string+data.toString();
					a = a.split(/\n/);
					overflow_string = "";
				}
    			for(var i=0;i<a.length;i++){
    				if(a[i].length > 0){
    					//console.log("arduino: "+a[i]); //log volts & amps
						var ob = {
							tag: "tx",
							message: a[i].substr(a[i].indexOf(":")+2, a[i].length) //tx: 08
    					}
						if(a[i].indexOf("rx") != -1){
							ob.tag = "rx";
						}
						if(a[i].length == 6)
							io.sockets.emit('log', ob);
						else {
							overflow_string = a[i];
							//console.log("incomplete packet: "+a[i]);
						}
    				}
    			}*/

  				//serialPort.write("ls\n", function(err, results) {
  				//  console.log('err ' + err);
  				//  console.log('results ' + results);
  				//});
			});
		});
	} catch(e){
		console.error(e);
	}
}
tty.list(function (err, ports) {
	console.log("#ports: "+ports.length);
	if(ports.length > 0){
		ports.forEach(function(port) {
			console.log(port.comName);
			//console.log(port.pnpId);
			//console.log(port.manufacturer);
		});
		prompt.get('portname', function (err, result) {
			if (err) { //return onErr(err); 
			}
			open_serial_port(result.portname);
		});
	} else {
		//serialPort = new tty.SerialPort("/dev/rfcomm0", {
	}
});	

process.on('SIGINT', function() {    
	 if(serialPort != null && serialPort.isOpen()){
	 	serialPort.close();
	 	console.log("Closing Com Port...");
	 }
    process.exit();
});
