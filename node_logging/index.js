var express = require('express');
var app = express();

var server_url = "127.0.0.1";
var server_port = 3001;
var io = require('socket.io').listen(3002);
var tty = require("serialport");
var fs = require('fs');


if (typeof localStorage === "undefined" || localStorage === null) {
	var LocalStorage = require('node-localstorage').LocalStorage;
	localStorage = new LocalStorage('./localstorage', 500 * 1024 * 1024); //50mb limit
	//localStorage._deleteLocation('./localstorage');  //cleans up ./localstorage created during doctest
	//localStorage.setItem('myFirstKey', JSON.stringify({test: 123.0})); 
	//console.log(localStorage.getItem('myFirstKey'));
}

//app.use( express.json({limit: '50mb'}) );
app.use( express.urlencoded({
  limit: '50mb',
  extended: true,
  parameterLimit:50000
}));
app.use('/assets', express.static('assets'));
app.get('/', function (req, res) {
	var path = req.params[0] ? req.params[0] : 'index.html';
   //res.sendFile(path, {root: './assets'});
   res.redirect('/assets/index.html');
});

app.get('/getKeys', function(req, res){
	var keys = new Array();
	for(var i=0; i<localStorage.length;i++){
		keys.push(localStorage.key(i));
	}
	res.write(JSON.stringify({keys: keys}));
	res.end();
});
app.get('/getItem', function(req, res){
	var key = req.query.key;
	//console.log("key: "+key);
	var item = localStorage.getItem(key);
	//console.log(item);
	res.write(item);
	res.end();
});

app.post('/saveItem', function(req, res){

	//console.log(req);
	//console.log("key: "+key);
	//console.log("req.query.data: "+data);	
	if(typeof(req.query.key) != 'undefined'){
		var key = req.query.key;
		var data = req.query.data;	
		console.log("_key: "+key);
		console.log("_data: "+data);
		if(typeof(data) != 'undefined'){
			localStorage.setItem(key, JSON.stringify(data));
			res.write('{"response": "success"}');
		} else {
			res.write('{"response": "error"}');
		}
		res.end();
	} else {
		var key = req.body.key;
		var data = req.body.data;
		console.log("key: "+key);
		console.log("data: "+data);	
		if(typeof(data) != 'undefined'){
			localStorage.setItem(key, JSON.stringify(data));
			res.write('{"response": "success"}');
		} else {
			res.write('{"response": "error"}');
		}
		res.end();	
	}
});
app.post('/', function(req, res){
	//console.log('Post recieved at http://%s:%s', server_url, server_port);
	//print(request, response);
	//console.log(req.query);
	//console.log(req.params);
	//console.log(request.originalUrl);
	//console.log(req.body);
	console.log(req.body.tag+": "+req.body.message);
		io.sockets.emit('log', {
         tag: req.body.tag,
         message: req.body.message
      });
   res.send({
   	status_code: 1,
   	message: ""
   });	
});
var server = app.listen(server_port, function () {
	  var host = server.address().address;
	  var port = server.address().port;

	  console.log('Example app listening at http://%s:%s', host, port);
});

var comPort = "COM1";
function hasPort(ports){
	ports.forEach(function(port) {
	  console.log(port.comName);
	  //console.log(port.pnpId);
	  //console.log(port.manufacturer);
		if(port.comName == comPort){
			return port.comName;
		}
	});
	return false;
}
try{
	//now the socketio server
	var serialPort = null;
	var baudrate = 115200;
	var overflow_string = "";
	tty.list(function (err, ports) {
	  console.log("#ports: "+ports.length);
	  //console.log(hasPort(ports));
	  if(ports.length > 0){
			//console.log("opening "+ports[ports.length-1].comName);
			//serialPort = new tty.SerialPort(ports[ports.length-1].comName, {
			serialPort = new tty.SerialPort(comPort, {
				baudrate: baudrate,
				bufferSize: 4096
			});
			serialPort.on("open", function () {
				//console.log("port open");
				serialPort.on('data', function(data) {
					//console.log(data);
					var a = data.toString();
					//console.log(a);
					a = a.split(/\n/);
					if(overflow_string.length > 0){
						a = overflow_string+data.toString();
						a = a.split(/\n/);
						overflow_string = "";
					}
					for(var i=0;i<a.length;i++){
						if(a[i].length > 0){
							console.log("arduino: "+a[i]); //
							var ob = {
								tag: "tx",
								message: a[i].substr(a[i].indexOf(":")+2, a[i].length) //tx: 08
							}
							if(a[i].indexOf("rx") != -1){
								ob.tag = "rx";
							}
							if(a[i].length == 6){
								io.sockets.emit('log', ob);
								//console.log(ob);
							}
							else {
								overflow_string = a[i];
								//console.log("incomplete packet: "+a[i]);
							}
						}
					}

					//serialPort.write("ls\n", function(err, results) {
					//  console.log('err ' + err);
					//  console.log('results ' + results);
					//});
				});
			});
		}/* else {
			serialPort = new tty.SerialPort("/dev/rfcomm0", {
				baudrate: baudrate
			});
			serialPort.on("open", function () {
			//console.log('open');
			serialPort.on('data', function(data) {
				console.log('bluetooth: ' + data);
				io.sockets.emit('log', {
					tag: "info",
					message: data+""
					});    
				});
				//serialPort.write("ls\n", function(err, results) {
				//  console.log('err ' + err);
				//  console.log('results ' + results);
				//});
			});		
		}*/
	});	
} catch(e){

}

io.on('connection', function (socket) {
	console.log("new connection...");

    socket.emit('log', {
        tag: "debug",
        message: "begin session..."
    });
});

process.on('SIGINT', function() {    
	 if(serialPort != null && serialPort.isOpen()){
	 	serialPort.close();
	 	console.log("Closing Com Port...");
	 }
    process.exit();
});
