/*

I spliced a display extension cable and used arduino Mega to intercept
serial communications.

cable I bought from http://www.ebay.com/itm/8fun-bafang-center-motor-mid-drive-motor-kit-display-extension-cable-/322187825892?var=&hash=item4b03e412e4:m:mF0vjpnXzTANDTToiH3yXNw

I connected each color wire to the corresponding wire on the other end, and added 3 wires gnd rx1 and rx2
which i pluged into arduino.

white: display tx, controller rx. 
green: display rx, controller tx.

Black (GND) arduino gnd, 
Green (TXD) serial3RX, 
White (RXD) serial2RX, 
Brown (P+),
Orange (PL)

USB Host: MAX3421E. The MAX3421E comunicate with Arduino with the SPI bus. So it uses the following pins:
Digital: 7 (RST), 50 (MISO), 51 (MOSI), 52 (SCK).
NB:Please do not use Digital pin 7 as input or output because is used in the comunication with MAX3421E

*/
void setup() {
  // put your setup code here, to run once:
  delay(200);
  Serial.begin(115200);
  Serial3.begin(1200);
  Serial2.begin(1200);
  
  delay(50);
}

void loop() {
 /*
  * Read bytes as fast as possible one at a time from each port,
  * this ensures data is synced as close as possible.
  */
  if(Serial2.available() > 0){
    Serial.print("tx: ");
    byte s2b = Serial2.read();
    if(s2b < 16)
      Serial.print("0");
    Serial.print(s2b, HEX);
    Serial.print("\n");    
  }
  if(Serial3.available() > 0){
    Serial.print("rx: ");
    byte s1b = Serial3.read();
    if(s1b < 16)
      Serial.print("0");    
    Serial.print(s1b, HEX);
    Serial.print("\n");
  }  
  //delay(2);
}
